# Конфигурация инструментов проверки соответствия код стайлу

### Внедрение в проект:

Скачать данный репозиторий и распаковать в папку `app`

#### Интеграция в CI:

Добавить в корневой gitlab-ci.yml: <br>
`include: '/app/flake8-ci.yaml'`

### Использование:

Всё выполняется из папки app рядом с конфиг файлами.

1) Установка зависимостей `pip install -r flake8-requirements.txt`

2) Запуск `python -m flake8`

3) Автоматический фикс импортов `python -m isort .`

##### Интеграция isort с редактором

https://github.com/pycqa/isort/wiki/isort-Plugins

##### Автоформатирование YAPF

###### Установка
```bash
pip install yapf
```
###### Использование
```bash
install_path/yapf --in-place --recursive --style config_path/.style.yapf file_path
```

Для детализации использования есть [документация](https://github.com/google/yapf)

###### Пример интеграции с pycharm

![Пример интеграции с pycharm](img/pycharm_integraion.png)

Program:
```
/home/user/.local/bin/yapf
```

Arguments:
```
--in-place --recursive --style $Projectpath$/.style.yapf $FilePath$
```

Working directory:
```bash
$Projectpath$
```

### Документирование кода
Документирование кода обязательно.
Для сборки документации предлагается использовать [sphinx](https://www.sphinx-doc.org/en/master/),
а для её заполнения [sphinx docstring format](https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html#the-sphinx-docstring-format).
По указанным ссылками приведены примеры для заполнения документации и её сборки.
